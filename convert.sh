#!/bin/bash

if [ $# -eq 0 ]
    then
    for w in `find *.wav -newermt "-2 hours"`
    do
        echo $w
        cp "$w" "$w-2"
        rm "$w"
        ffmpeg -i $w-2 -vn -ar 16000 -ac 2 -b:a 48k -f mp3 $w.mp3
        rm "$w-2"
    done
    for f in `find *.mp3 -newermt "-2 hours"`
    do
        echo $f
        cp "$f" "$f-2"
        rm "$f"
        ffmpeg -i "$f-2" -ac 2 -codec:a libmp3lame -b:a 48k -ar 16000 "$f"
        rm "$f-2"
    done
else

        f=$1
        echo $f
        cp "$f" "$f-2"
        rm "$f"
        ffmpeg -i "$f-2" -ac 2 -codec:a libmp3lame -b:a 48k -ar 16000 "$f"
        rm "$f-2"
fi

